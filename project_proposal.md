# Project Proposal

## Team

128554 Rron Shabani\
128588 Ali Amzai\
128727 Meriton Ademi

## Overview

The main goal of this project is to showcase user authentication using AWS Cognito. We want to include multiple user roles that will be part of auth tokens, as oposed to user role databases. The project will include a simple front end with sign in and sign up screens as well as demo pages for authenticated users. The cloud architecture will be configured with the Serverless Framework.

## Requirements

- Authentication using AWS Cognito
- User Roles using pre tokens
- Automated cloud architecture configuration

## Technology Stack

- AWS
- AWS Cognito
- Serverless Framework (YAML)
- React
