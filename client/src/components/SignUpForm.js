import React, { useState } from "react";
import "./LoginForm.css"
import UserPool from "./UserPool"

const SignUpForm = (_props) => {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const onSubmit = event => {
    event.preventDefault();

    UserPool.signUp(email, password, [], null, (err, data) => {
      if (err) console.error(err);
      console.log(data);
    });
   };

  return (
    <form onSubmit={onSubmit}>
      <input 
        value={email}  
        onChange={(event) => setEmail(event.target.value)}
      />
      <input 
        value={password}  
        onChange={(event) => setPassword(event.target.value)}
      />
      <button type="submit">Sign Up</button>
    </form>
  );
}

export default SignUpForm;
