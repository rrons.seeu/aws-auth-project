import { AuthenticationDetails, CognitoUser } from "amazon-cognito-identity-js";
import React, { useState } from "react";
import "./LoginForm.css"
import UserPool from "./UserPool"

const LoginForm = (_props) => {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const onSubmit = event => {
    event.preventDefault();
    // console.log(event.target);

    const user = new CognitoUser({
        Username: email,
        Pool: UserPool
      });

    const authDetails = new AuthenticationDetails({
      Username: email,
      Password: password
    });

    user.authenticateUser(authDetails, {
      onSuccess: data => {
        console.log("onSuccess:", data);
      },
      onFailure: data => {
        console.error("onFailure:", data);
      },
      newPasswordRequired: data => {
        console.log("onSuccess:", data);
      }
    })
  }

  return (
    <form onSubmit={onSubmit}>
      <input 
        value={email}  
        onChange={(event) => setEmail(event.target.value)}
      />
      <input 
        value={password}  
        onChange={(event) => setPassword(event.target.value)}
      />
      <button type="submit">Log In</button>
    </form>
  );
}

export default LoginForm;
