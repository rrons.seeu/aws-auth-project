import SignUpForm from  "./components/SignUpForm";
import LoginForm from "./components/LoginForm";

function App() {
  return (
    <div className="App">
      <SignUpForm />
      <LoginForm />
    </div>
  );
}

export default App;
